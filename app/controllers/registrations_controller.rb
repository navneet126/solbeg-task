class RegistrationsController < Devise::RegistrationsController
  #to update the details of the user without enter password	
  protected
  def update_resource(resource, params)
    resource.update_without_password(params)
  end
end
