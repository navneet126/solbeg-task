class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception

	
    before_action :configure_permitted_parameters, if: :devise_controller?
    

    #updated the params of the devose to register and to update the user details
    protected
    	def configure_permitted_parameters
	    	devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :password, :address, :phone,:picture])
	    	devise_parameter_sanitizer.permit(:account_update, keys: [:name, :email, :password, :address, :phone,:picture])
	  	end
end
