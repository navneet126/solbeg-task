class UserController < ApplicationController
  #controller to update each user data by admin

  #method to get the edit page details per user
	def edit
     @user = User.find(params[:id])
   end

   #method to to update user details
   def update
     @user = User.find(params[:id])
     if @user.update(user_params)
       redirect_to root_path
     else
       render json: @user, status: :error
     end
   end
   private
  
  #Params of the tabel user
  def user_params
    params.require(:user).permit(:name, :email, :address, :phone,:picture)
  end
end
